package phd.chromiak.dias.mediator.domain.model;

import lombok.Data;
import lombok.Getter;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;

/**
 * Created by Michal Chromiak on 05.11.14.
 */

@Data
@NodeEntity
public class EmployeeBase {

    @GraphId
    private Long nodeID;

    private Integer neo_eb_id;

    private Integer neo_eb_bossID;
    private String  neo_eb_name;

    public Integer getNeo_eb_id() {
        return neo_eb_id;
    }

    public Integer getNeo_eb_bossID() {
        return neo_eb_bossID;
    }

    public String getNeo_eb_name() {
        return neo_eb_name;
    }



/*    @Override
    public String toString() {
        return neo_eb_id +", "+ neo_eb_bossID +", "+ neo_eb_name ;
    }*/
}
