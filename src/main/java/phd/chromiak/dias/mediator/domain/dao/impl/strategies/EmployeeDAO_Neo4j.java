package phd.chromiak.dias.mediator.domain.dao.impl.strategies;

import org.neo4j.graphdb.Path;
import org.springframework.data.neo4j.conversion.Result;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.stereotype.Repository;
import phd.chromiak.dias.mediator.domain.dao.EmployeeDAO;
import phd.chromiak.dias.mediator.domain.model.EmployeeBase;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Michal Chromiak on 02.11.14.
 */

@Repository
public class EmployeeDAO_Neo4j implements EmployeeDAO {

    @Inject
    private Neo4jTemplate neo4jTemplate;


    @Override
    public List<Map<String, Object>> findAll(String query) {

        //Object is the node address
        long start_time, end_time;
        double difference;
        start_time = System.currentTimeMillis();
        Result<Map<String, Object>> result = neo4jTemplate.query(query, null);
        end_time = System.currentTimeMillis();
        difference = (end_time - start_time);
        System.out.println("[Neo4j] Zapytanie trwało: " +difference);

        //List<Map<String, Object>> mapList = IteratorUtil.asList(result.to(EmployeeBase.class).as(List.class));


        List<Map<String, Object>> table = new ArrayList<Map<String, Object>>();
        Map<String, Object> row = null;


        //this loop is used when querying for recursive structure over neo4j
        //need to use Neo4j java classed to query down the hierarchy
        for(Object u : result.to(Object.class)) {


            row = new HashMap<>();
            row.put("rel",        u);

            table.add(row);

        }


/* //this loop is used when not querying for recursive structure over neo4j; most cases
        for(EmployeeBase u : result.to(EmployeeBase.class)) {

            row = new HashMap<>();
            row.put("neo_eb_id",        u.getNeo_eb_id());
            row.put("neo_eb_bossID", u.getNeo_eb_bossID());
            row.put("neo_eb_name",      u.getNeo_eb_name());

            table.add(row);

        }
*/

        //System.out.println(result.to(EmployeeBase.class).as(List.class).get(1));
        // System.out.println(neo4jTemplate.getNode(0).getProperty("neo_eb_id"));
        //System.out.println("WYniki:"+ result.as(List.class).get(1)+"SIZE:"+result.as(List.class).size());
        return table;
    }
}
