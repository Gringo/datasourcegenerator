package phd.chromiak.dias.mediator.domain.persitence;

import java.util.List;
import java.util.Map;

/**
 * Created by Michal Chromiak on 28.10.14.
 */

public interface EmpRepository  {

    List<Map<String, Object>> getAllEmpRecords(String tableName, String schemaName, String strategyName);
    List<Map<String, Object>> getLimited(String tableName, String schemaName, String strategyName, Integer limit, Long off);

}
