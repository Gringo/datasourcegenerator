package phd.chromiak.dias.mediator.domain.model;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.NodeEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Cross-store persistance entity using transient annotation
 * Created by Michal Chromiak on 09.11.14.
 */

@Entity
@Table(name = "empful")
@NodeEntity(partial = true)
public class EmpMix {

    @GraphId
    private Long nodeID;

    @GraphProperty
    private EmployeeBase employeeBase;

    private String w_s1;
    private String w_s2;
    private String w_s3;
    private String w_s4;
    private String w_s5;
}
