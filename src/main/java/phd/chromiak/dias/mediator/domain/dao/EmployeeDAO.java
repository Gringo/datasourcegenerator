package phd.chromiak.dias.mediator.domain.dao;

import java.util.List;
import java.util.Map;

/**
 * Created by Michal Chromiak on 29.10.14.
 */

/* Interface for different data access methods: JdbcTemplate for relational DB, other for NoSQL*/


public interface EmployeeDAO {

    public List<Map<String, Object>> findAll(String query);
}
