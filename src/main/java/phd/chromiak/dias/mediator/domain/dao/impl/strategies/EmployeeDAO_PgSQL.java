package phd.chromiak.dias.mediator.domain.dao.impl.strategies;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import phd.chromiak.dias.mediator.domain.dao.EmployeeDAO;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * Created by Michal Chromiak on 02.11.14.
 */

@Repository
public class EmployeeDAO_PgSQL implements EmployeeDAO {

    @Inject
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Map<String, Object>> findAll(String query){

        long start_time, end_time;
        double difference;
        start_time = System.currentTimeMillis();

        List<Map<String, Object>> l = jdbcTemplate.queryForList(query);

        end_time = System.currentTimeMillis();
        difference = (end_time - start_time);
        System.out.println("[PgSQL] Zapytanie trwało: " +difference);

/*        for (Map m : l){
            System.out.println("String: "+ m.keySet()+"; Object: klasa="+m.getClass().toString()+" val="+m.values());
        }*/
        return l;
    }
}
