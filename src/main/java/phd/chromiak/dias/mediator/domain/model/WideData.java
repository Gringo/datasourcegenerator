package phd.chromiak.dias.mediator.domain.model;

import lombok.Data;

/**
 * Created by Michal Chromiak on 05.11.14.
 */
@Data
public class WideData {

    private Long w_id;
    private String w_s1;
    private String w_s2;
    private String w_s3;
    private String w_s4;
    private String w_s5;
}
