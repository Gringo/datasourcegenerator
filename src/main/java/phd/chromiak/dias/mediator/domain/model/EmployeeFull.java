package phd.chromiak.dias.mediator.domain.model;

import lombok.Data;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;

/**
 * Created by Michal Chromiak on 28.10.14.
 */
@Data
@NodeEntity
public class EmployeeFull {

    @GraphId
    private Long nodeID;

    private Integer ef_bossId;
    private String  ef_name;

    private String ef_s1;
    private String ef_s2;
    private String ef_s3;
    private String ef_s4;
    private String ef_s5;

}
