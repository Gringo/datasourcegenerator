package phd.chromiak.dias.mediator.domain.persitence.impl;

import org.springframework.stereotype.Service;
import phd.chromiak.dias.mediator.domain.persitence.EmpRepository;
import phd.chromiak.dias.mediator.domain.persitence.Finder;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * Created by Michal Chromiak on 28.10.14.
 */

@Service
public class EmpRepositoryImpl implements EmpRepository {

    @Inject
    Finder finder;

    @Override
    public List<Map<String, Object>> getAllEmpRecords(String tableName, String schemaName, String strategyName) {

        if (strategyName.equals("mix") ){
            return finder.findAllmix(tableName,schemaName);
        }else {
            return finder.findAll(tableName, schemaName, strategyName);
        }

    }

    @Override
    public List<Map<String, Object>> getLimited(String tableName, String schemaName, String strategyName,
                                          Integer limit, Long off) {
        if (strategyName.equals("mix") ){
            return finder.findMixRecLTD(tableName, schemaName, limit, off);
        }else {
            //return finder.findLimited(tableName, schemaName, strategyName, limit, off);
            return finder.findRecEmpBaseWideDataLTD(tableName, schemaName, limit, off);
        }
    }
}
