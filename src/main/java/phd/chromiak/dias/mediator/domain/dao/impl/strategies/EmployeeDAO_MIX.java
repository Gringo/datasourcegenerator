package phd.chromiak.dias.mediator.domain.dao.impl.strategies;

import org.springframework.stereotype.Repository;
import phd.chromiak.dias.mediator.domain.dao.EmployeeDAO;

import java.util.*;

import static java.util.stream.Collectors.toMap;

/**
 * Created by Michal Chromiak on 03.11.14.
 */

@Repository
public class EmployeeDAO_MIX implements EmployeeDAO {
    private List<Map<String, Object>> neoTable_empBase;
    private List<Map<String, Object>> pgTable_wide;


    @Override
    public List<Map<String, Object>> findAll(String query) {
        return null;
    }

    public List<Map<String, Object>> findAllMix( List<Map<String, Object>> pgList, List<Map<String, Object>> neoList){

        this.neoTable_empBase    = neoList;
        this.pgTable_wide        = pgList;

        return joinOnBRI();

    }

    public List<Map<String, Object>> joinOnBRI() {

        List<Map<String, Object>> tableMix = new ArrayList<Map<String, Object>>();
        Map<String, Object> recordMix = null;

        for(int i = 0; i < neoTable_empBase.size(); i++)
        {
            recordMix= new HashMap<String, Object>();
            recordMix.putAll(neoTable_empBase.get(i));
            recordMix.putAll(pgTable_wide.get(i));
            tableMix.add(recordMix);
        }
        return tableMix;

        /*Funkcyjnie
        joinedList.forEach( neoList.stream().collect())

                .entrySet().stream()
                .collect(toMap(e -> e.getValue(),
                        e -> mapTwo.get(e.getKey())));*/

    }

}
