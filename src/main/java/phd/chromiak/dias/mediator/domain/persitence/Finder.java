package phd.chromiak.dias.mediator.domain.persitence;

import org.springframework.stereotype.Service;
import phd.chromiak.dias.mediator.domain.dao.EmployeeDAO;
import phd.chromiak.dias.mediator.domain.dao.impl.strategies.EmployeeDAO_MIX;
import phd.chromiak.dias.mediator.domain.dao.impl.strategies.EmployeeDAO_Neo4j;
import phd.chromiak.dias.mediator.domain.dao.impl.strategies.EmployeeDAO_PgSQL;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Map;

/**
 * Created by Michal Chromiak on 29.10.14.
 */

@Service
public class Finder {


/*zrobić zamiast 3xinject to Applicationcontext aware albo wstrzynięcie ctx */

    @Inject
    private EmployeeDAO_PgSQL employeeDAO_pgSQL;
    @Inject
    private EmployeeDAO_Neo4j employeeDAO_neo4j;
    @Inject
    private EmployeeDAO_MIX employeeDAO_mix;


    public Finder(){};

    public List<Map<String, Object>> findAll(String tableName, String schemaName, String strategyName){

        EmployeeDAO employeeDAO=null;
        String query=null;
        List<Map<String, Object>> result;

        switch (Strategy.toStrategy(strategyName)){
            case MIX:

                break;
            case NEO:   //Neo4j ONLY
                query = "MATCH (emp:`"+tableName+"`) RETURN emp;";
                employeeDAO = employeeDAO_neo4j;
                break;
            case PG:    //PG ONLY
                query = "SELECT * FROM \""+schemaName+"\"."+tableName+";";
                employeeDAO = employeeDAO_pgSQL;
                break;
        }

        return employeeDAO.findAll(query);
    }

    public List<Map<String, Object>> findLimited(String tableName, String schemaName, String strategyName, Integer limit, Long off){

        EmployeeDAO employeeDAO=null;
        String query=null;
        List<Map<String, Object>> result;

        switch (Strategy.toStrategy(strategyName)){
            case MIX:

                break;
            case NEO:   //Neo4j ONLY
                query = "MATCH (emp:`"+tableName+"`) RETURN emp SKIP "+off+" LIMIT "+limit+";";
                employeeDAO = employeeDAO_neo4j;
                break;
            case PG:    //PG ONLY
                query = "SELECT * FROM \""+schemaName+"\"."+tableName+" LIMIT "+limit+" OFFSET "+off+";";
                employeeDAO = employeeDAO_pgSQL;
                break;
        }

        return employeeDAO.findAll(query);
    }

    public List<Map<String, Object>> findAllmix(String tableName, String schemaName){

        String neoQ =   "MATCH (emp:`"+tableName+"`) RETURN emp;";
        String pgQ  =   "SELECT * FROM \""+schemaName+"\".widedata;";

        return employeeDAO_mix.findAllMix(employeeDAO_pgSQL.findAll(pgQ),employeeDAO_neo4j.findAll(neoQ));

    }


    /* TEST 1 -Query recursively EmpFull table in PgSQL
     */
    public List<Map<String, Object>> findRecEmpFullLTD(String tableName, String schemaName, Integer limit, Long off){
        String pgQrec = "WITH RECURSIVE Emp_CTE AS (\n" +
                "\t\tSELECT e_id, e_name, e_bossid, w_id, w_s1, w_s2, w_s3, w_s4, w_s5\n" +
                "\t\tFROM public.empfull\n" +
                "\t\tWHERE e_bossid IS NULL\n" +
                "\t\tUNION ALL\n" +
                "\t\tSELECT e.e_id, e.e_name, e.e_bossid, e.w_id, e.w_s1, e.w_s2, e.w_s3, e.w_s4, e.w_s5\n" +
                "\t\tFROM public.empfull e\n" +
                "\t\tINNER JOIN Emp_CTE ecte ON ecte.e_id = e.e_bossid\n" +
                ")\n" +
                "\n" +
                "SELECT *\n" +
                "FROM Emp_CTE LIMIT "+limit+" OFFSET "+off+";";

        return employeeDAO_pgSQL.findAll(pgQrec);
    }

    /* TEST 2 -Query recursivelly EmpBase and WideData tables form PgSQL*/
    public List<Map<String, Object>> findRecEmpBaseWideDataLTD(String tableName, String schemaName, Integer limit, Long off){
        String pgQ1 = "WITH RECURSIVE Emp_CTE AS (\n" +
                "\t\tSELECT e_id, e_name, e_bossid\n" +
                "\t\tFROM public.empbase\n" +
                "\t\tWHERE e_bossid IS NULL\n" +
                "\t\tUNION ALL\n" +
                "\t\tSELECT e.e_id, e.e_name, e.e_bossid\n" +
                "\t\tFROM public.empbase e\n" +
                "\t\tINNER JOIN Emp_CTE ecte ON ecte.e_id = e.e_bossid\n" +
                ")\n" +
                "\n" +
                "SELECT *\n" +
                "FROM Emp_CTE LIMIT "+limit+" OFFSET "+off+";";

        String pgQ2  =   "SELECT * FROM \""+schemaName+"\".widedata LIMIT "+limit+" OFFSET "+off+";";

        return employeeDAO_mix.findAllMix(employeeDAO_pgSQL.findAll(pgQ1),employeeDAO_pgSQL.findAll(pgQ2));
    }

    /*TEST 3 - Query recursivelly EmpBase nodes in Neo4j and glue with WideData from PgSQL */
    public List<Map<String, Object>> findMixRecLTD(String tableName, String schemaName, Integer limit, Long off){

        String neoQ =   "MATCH path = (b:Root)<-[:REPORTS_TO*]-(e:EmpBase)\n" +
                        "RETURN nodes(path)\n" +
                        "LIMIT "+limit+";";
        String pgQ  =   "SELECT * FROM \""+schemaName+"\".widedata LIMIT "+limit+" OFFSET "+off+";";

        return employeeDAO_mix.findAllMix(employeeDAO_pgSQL.findAll(pgQ),employeeDAO_neo4j.findAll(neoQ));
    }

    public List<Map<String, Object>> findMixLimited(String tableName, String schemaName, Integer limit, Long off){

        String neoQ =   "MATCH (emp:`"+tableName+"`) RETURN emp SKIP "+off+" LIMIT "+limit+";";
        String pgQ  =   "SELECT * FROM \""+schemaName+"\".widedata LIMIT "+limit+" OFFSET "+off+";";

        return employeeDAO_mix.findAllMix(employeeDAO_pgSQL.findAll(pgQ),employeeDAO_neo4j.findAll(neoQ));
    }



    public enum Strategy {
        MIX, NEO, PG, NOVALUE;

        public static Strategy toStrategy(String str) {
            try {
                return valueOf(str.toUpperCase());
            }
            catch (Exception ex) {
                return NOVALUE;
            }
        }
    }
}
