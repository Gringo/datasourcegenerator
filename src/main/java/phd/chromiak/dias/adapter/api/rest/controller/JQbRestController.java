package phd.chromiak.dias.adapter.api.rest.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import phd.chromiak.dias.mediator.domain.persitence.impl.EmpRepositoryImpl;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * Created by Michal Chromiak on 30.10.14.
 */

@RestController
@RequestMapping("/db")
public class JQbRestController {

    @Inject
    private EmpRepositoryImpl empRepository;


    @RequestMapping(value = "/{strategyName}/{schemaName}/{tableName}.json",
                    method = RequestMethod.GET,
                    produces = "application/json")

    public  List<Map<String, Object>> getTableRecords(@PathVariable String strategyName,
                                  @PathVariable String tableName,
                                  @PathVariable String schemaName){

        List<Map<String, Object>> resultList = empRepository.getAllEmpRecords(tableName, schemaName, strategyName);

        return resultList;
    }

    @RequestMapping(value = "/{strategyName}/{schemaName}/{tableName}.json/limit={limit}&idoffset={offset}",
                   method = RequestMethod.GET,
                   produces = "application/json")

    public  List<Map<String, Object>> getTableRecordsLim(   @PathVariable String strategyName,
                                                            @PathVariable String tableName,
                                                            @PathVariable String schemaName,
                                                            @PathVariable Integer limit,
                                                            @PathVariable Long offset){

        List<Map<String, Object>> resultList = empRepository.getLimited(tableName,schemaName,strategyName,limit,offset);

        return resultList;
    }

}
