-- EMP BASE
INSERT INTO "EMP_SCHEMA".empbase (e_id, e_name, e_bossid)
VALUES  (11,  'Smith',    11),
  (12,  'Johnson',  11),
  (13,  'Roberts',  11),
  (14,  'Doe',      13);

-- EMP FULL
INSERT INTO "EMP_SCHEMA".empful (ef_id, ef_name, ef_bossid, ef_s1, ef_s2, ef_s3, ef_s4, ef_s5)
VALUES   (11,  'Smith',   11,  '11string1', '11string2','11string3','11string4','11string5'),
  (12,  'Johnson', 11,  '12string1', '12string2','12string3','12string4','12string5'),
  (13,  'Roberts', 11,  '13string1', '13string2','13string3','13string4','13string5'),
  (14,  'Doe',     13,  '14string1', '14string2','14string3','14string4','14string5');

-- WIDE
INSERT INTO "EMP_SCHEMA".widedata (w_id, w_s1, w_s2, w_s3, w_s4, w_s5)
VALUES  (11,  '11string1', '11string2','11string3','11string4','11string5'),
  (12,  '12string1', '12string2','12string3','12string4','12string5'),
  (13,  '13string1', '13string2','13string3','13string4','13string5'),
  (14,  '14string1', '14string2','14string3','14string4','14string5');