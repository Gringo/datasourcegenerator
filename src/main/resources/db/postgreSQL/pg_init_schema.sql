-- Script that generates database schema
DROP SCHEMA IF EXISTS "EMP_SCHEMA" CASCADE;
CREATE SCHEMA "EMP_SCHEMA" AUTHORIZATION mc;

--SET SCHEMA "FACT_SCHEMA";

DROP TABLE IF EXISTS "EMP_SCHEMA".empbase;
DROP TABLE IF EXISTS "EMP_SCHEMA".empful;
DROP TABLE IF EXISTS "EMP_SCHEMA".widedata;

CREATE TABLE "EMP_SCHEMA".empbase(
  e_id  SERIAL PRIMARY KEY,
  e_bossId INTEGER,
  e_name TEXT,
  FOREIGN KEY (e_bossId) REFERENCES "EMP_SCHEMA".empbase(e_id)
);

CREATE TABLE "EMP_SCHEMA".empful(
  ef_id  SERIAL PRIMARY KEY,
  ef_bossId INTEGER,
  ef_name TEXT,
  ef_s1 TEXT,
  ef_s2 TEXT,
  ef_s3 TEXT,
  ef_s4 TEXT,
  ef_s5 TEXT
);

CREATE TABLE "EMP_SCHEMA".widedata(
  w_id  SERIAL PRIMARY KEY,
  w_s1 TEXT,
  w_s2 TEXT,
  w_s3 TEXT,
  w_s4 TEXT,
  w_s5 TEXT
);

